package com.cornel.lab8.src.instruments;

import com.cornel.lab8.src.matrices.UsualMatrix;

import java.util.ArrayList;
import java.util.logging.Logger;

/**
 * Реализовать класс ParallelMatrixProduct для многопоточного
 * умножения матриц matrices.UsualMatrix. В конструкторе класс получает число потоков,
 * которые будут использованы для перемножения (число потоков может быть меньше,
 * число строк у первой матрицы).
 * В функции main сравнить время перемножения больших случайных матриц
 * обычным и многопоточным способом.
 * Получить текущее время можно с помощью методов класса System.
 * */

public class ParallelMatrixProduct {
    private final int threadCount;
    private ArrayList<Thread> threads = new ArrayList<>();
    private Logger logger = Logger.getLogger(this.getClass().toString());

    public ParallelMatrixProduct(int threadCount) {
        this.threadCount = threadCount;
    }

    public UsualMatrix product(UsualMatrix m1, UsualMatrix m2) throws InterruptedException {
        if (m1.getWidth() != m2.getHeight()) {
            throw new IllegalArgumentException("Can't produce matrices: m1 width must be equal to m2 height");
        }

        UsualMatrix result = new UsualMatrix(m1.getHeight(), m2.getWidth());

        float cellCount = result.getHeight() * result.getWidth();
        int cellPerThread = (int) Math.ceil(cellCount / threadCount);

        Thread thread;
        for (int i = 0; i < result.getElementCount(); i += cellPerThread) {
            thread = new Thread(new MatrixPartCalc(i, cellPerThread, m1, m2, result));
            threads.add(thread);
        }

        for (Thread t : threads) { t.start(); }

        for (Thread t : threads) {
            if (t.isAlive()) {
                t.join();
            }
        }
        return result;
    }

    private class MatrixPartCalc implements Runnable {
        int from;
        int to;
        private final UsualMatrix m1;
        private final UsualMatrix m2;
        private final UsualMatrix result;

        MatrixPartCalc(int i, int count, UsualMatrix m1, UsualMatrix m2, UsualMatrix result) {
            this.from = i;
            this.to = i + count;
            this.m1 = m1;
            this.m2 = m2;
            this.result = result;
        }

        @Override
        public void run() {
            int i;
            int j;
            int tmp;
            logger.info("Thread start");
            for (int index = from; index < to && index < result.getElementCount(); index++) {
                tmp = 0;
                i = index / result.getWidth();
                j = index % result.getWidth();

                for (int k = 0; k < m1.getWidth(); k++) {
                    tmp += m1.getElement(i, k) * m2.getElement(k, j);
                }

                result.setElement(index, tmp);
            }
            logger.info("Thread end");
        }
    }

}
