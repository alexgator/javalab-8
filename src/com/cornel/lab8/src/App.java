package com.cornel.lab8.src;

import com.cornel.lab8.src.matrices.UsualMatrix;
import com.cornel.lab8.src.instruments.ParallelMatrixProduct;

import java.util.logging.Level;
import java.util.logging.Logger;

public class App {
    private static final int M1_H       = 1000;
    private static final int M1_W       = 1000;
    private static final int M2_W       = 1000;
    private static final int THREADS    = 4;

    private static Logger logger = Logger.getLogger(App.class.toString());

    public static void main(String[] args) {
        try {
            println("Testing matrices production speed\n");

            UsualMatrix m1 = new UsualMatrix(M1_H, M1_W);
            UsualMatrix m2 = new UsualMatrix(M1_W, M2_W);
            m1.randomize(0, 10);
            m2.randomize(0, 10);

            println("The first matrix is " + m1.getHeight() + "x" + m1.getWidth());
            println(m1.toString());
            println("The second is " + m2.getHeight() + "x" + m2.getWidth());
            println(m2.toString());

            long start;
            long end;
            println("Producing via main thread...");
            start = time();
            UsualMatrix result1 = m1.product(m2);
            end = time();
            println("Completed. Estimated time is " + (end - start) + "ms");

            println("Producing with multithreading...");
            ParallelMatrixProduct producer = new ParallelMatrixProduct(THREADS);
            start = time();
            UsualMatrix result2 = producer.product(m1, m2);
            end = time();
            println("Completed. Estimated time is " + (end - start) + "ms");
            println(result1.toString());
            println("Results are equal:" + result1.equals(result2));

        } catch (InterruptedException e) {
            logger.log(Level.WARNING, "Error: process interrupted!");
            Thread.currentThread().interrupt();
        }
    }

    private static long time() {
        return System.currentTimeMillis();
    }

    private static void println(String printable) {
        logger.log(Level.INFO, "\n" + printable);
    }

}
