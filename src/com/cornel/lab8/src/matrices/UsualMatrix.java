package com.cornel.lab8.src.matrices;

import com.cornel.lab8.src.exception.MatrixException;

import java.util.Arrays;
import java.util.Random;

public class UsualMatrix {
    private int height;
    private int width;
    private int[][] matrix;

    public UsualMatrix(final int height, final int width) {
        if (!(height > 0 && width > 0)) {
            throw new MatrixException("Parameters height and width can't be less or equal to 0");
        }
        this.height = height;
        this.width = width;
        this.matrix = new int[height][width];
    }

    public int getElementCount() {
        return getWidth() * getHeight();
    }

    public int getHeight() {
        return this.height;
    }

    public int getWidth() {
        return this.width;
    }

    private void setElement(int row, int col, int value) {
        if (row < 0 || col < 0) {
            throw new MatrixException("Parameters row and col can't be less than 0");
        }
        if (row >= this.getHeight() || col >= this.getWidth()) {
            throw new MatrixException("Matrix index out of bounds");
        }
        this.matrix[row][col] = value;
    }

    public int getElement(int row, int col) {
        if (row < 0 || col < 0) {
            throw new MatrixException("Parameters row and col can't be less than 0");
        }
        if (row >= this.getHeight() || col >= this.getWidth()) {
            throw new MatrixException("Matrix index out of bounds");
        }
        return this.matrix[row][col];
    }

    public void setElement(int index, int value) {
        int row = index / this.getWidth();
        int col = index % this.getWidth();
        setElement(row, col, value);
    }

    public UsualMatrix product(final UsualMatrix operand) {
        if (this.getWidth() != operand.getHeight()) {
            throw new MatrixException("Operand height must be equal to the matrix width to product");
        }
        UsualMatrix result = new UsualMatrix(this.getHeight(), operand.getWidth());
        int tmp;
        for (int i = 0; i < result.getHeight(); i++) {
            for (int j = 0; j < result.getWidth(); j++) {
                tmp = 0;
                for (int k = 0; k < this.getWidth(); k++) {
                    tmp += this.getElement(i, k) * operand.getElement(k, j);
                }
                result.setElement(i, j, tmp);
            }
        }
        return result;
    }

    public void randomize(int from, int to) {
        Random random = new Random();
        for(int i = 0; i < getHeight(); i++) {
            for (int j = 0; j < getWidth(); j++) {
                setElement(i, j, random.nextInt((to - from) + 1) + from);
            }
        }
    }

    @Override
    public String toString() {
        StringBuilder output = new StringBuilder();
        for (int i = 0; i < this.getHeight(); i++) {
            output.append("| ");
            for (int j = 0; j < this.getWidth(); j++) {
                output.append(String.valueOf(this.getElement(i, j)));
                output.append(" ");
            }
            output.append("|\n");
        }
        return output.toString();
    }

    @Override
    public int hashCode() {
        int result = height;
        result = 31 * result + width;
        result = 31 * result + Arrays.deepHashCode(matrix);
        return result;
    }

    @Override
    public boolean equals(final Object operand) {
        if (!(operand instanceof UsualMatrix)) {
            return false;
        }
        if (this == operand) {
            return true;
        }

        UsualMatrix opCast = (UsualMatrix)operand;

        if (this.getHeight() != opCast.getHeight() || this.getWidth() != opCast.getWidth()) {
            return false;
        }
        for (int i = 0; i < this.getHeight(); i++) {
            for (int j = 0; j < this.getWidth(); j++) {
                if (this.getElement(i, j) != opCast.getElement(i, j)) {
                    return false;
                }
            }
        }
        return true;
    }
}


