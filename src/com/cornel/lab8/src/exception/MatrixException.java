package com.cornel.lab8.src.exception;

public class MatrixException extends RuntimeException{
    public MatrixException(String message) {
        super(message);
    }
}
